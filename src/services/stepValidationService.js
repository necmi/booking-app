import * as bookingValidation from "./bookingValidationService";

export function stepValidation(stepName) {
  let validationData = bookingValidation.checkAllValidation();
  let result = validationData.errors.filter(error => {
    return stepName === error.type;
  });

  return result;
}
