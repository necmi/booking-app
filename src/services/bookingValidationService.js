import { getAllBookingData } from "./localStorageService";

export function dateValidation(checkindate, checkoutdate) {
  let result = { status: true, errors: [] };

  if (checkindate === undefined || checkindate === "") {
    result.status = false;
    let error = {
      type: "date",
      text: "Check-in Tarihi Girilmesi zorunludur !"
    };
    result.errors.push(error);
  }

  if (checkoutdate === undefined || checkoutdate === "") {
    result.status = false;
    let error = {
      type: "date",
      text: "Check-out Tarihi Girilmesi zorunludur !"
    };
    result.errors.push(error);
  }

  return result;
}

export function roomValidation(room, landscape) {
  let result = { status: true, errors: [] };
  if (room === undefined || room === "") {
    result.status = false;
    let error = {
      type: "room",
      text: "Oda Tipi seçimi zorunludur !"
    };
    result.errors.push(error);
  }

  if (landscape === undefined || landscape === "") {
    result.status = false;
    let error = {
      type: "room",
      text: "Manzara seçimi zorunludur !"
    };
    result.errors.push(error);
  }

  return result;
}

export function paymentValidation(cvc, expiry, name, number) {
  let result = { status: true, errors: [] };

  if (cvc === undefined || cvc === "") {
    result.status = false;
    let error = {
      type: "payment",
      text: "Cvc girilmesi zorunludur !"
    };
    result.errors.push(error);
  }

  if (expiry === undefined || expiry === "") {
    result.status = false;
    let error = {
      type: "payment",
      text: "Son Kul. Tarihi girilmesi zorunludur !"
    };
    result.errors.push(error);
  }

  if (name === undefined || name === "") {
    result.status = false;
    let error = {
      type: "payment",
      text: "Ad Soyad girilmesi zorunludur !"
    };
    result.errors.push(error);
  }

  if (number === undefined || number === "") {
    result.status = false;
    let error = {
      type: "payment",
      text: "Kart Numarası girilmesi zorunludur !"
    };
    result.errors.push(error);
  }

  if (number !== undefined && number.toString().length > 19) {
    result.status = false;
    let error = {
      type: "payment",
      text: "Kart Numarası 11 karakterden uzun olamaz !"
    };
    result.errors.push(error);
  }
  return result;
}

export function checkAllValidation() {
  let result = { status: true, errors: [] };

  let allBookingData = getAllBookingData();
  let dateResult = dateValidation(
    allBookingData.date.checkindate,
    allBookingData.date.checkoutdate
  );
  let roomResult = roomValidation(
    allBookingData.room.room,
    allBookingData.room.landscape
  );
  let paymentResult = paymentValidation(
    allBookingData.payment.cvc,
    allBookingData.payment.expiry,
    allBookingData.payment.name,
    allBookingData.payment.number
  );

  if (!dateResult.status) {
    result.status = false;
    result.errors.push(...dateResult.errors);
  }

  if (!roomResult.status) {
    result.status = false;
    result.errors.push(...roomResult.errors);
  }

  if (!paymentResult.status) {
    result.status = false;
    result.errors.push(...paymentResult.errors);
  }

  return result;
}
