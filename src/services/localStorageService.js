const bookingKey = "booking-app";

export function configureLS() {
  var data = localStorage.getItem(bookingKey);
  if (data == null) {
    localStorage.setItem(
      bookingKey,
      JSON.stringify({
        date: {
          checkindate: "",
          checkoutdate: ""
        },
        room: {
          room: "",
          landscape: ""
        },
        payment: {
          number: "",
          name: "",
          expiry: "",
          cvc: ""
        }
      })
    );
  }
}

export function setAllItem(value) {
  let stringData = JSON.stringify(value);
  localStorage.setItem(bookingKey, stringData);
}

export function setSubItem(itemName, subItemName, value) {
  var data = localStorage.getItem(bookingKey);
  if (data == null) {
    configureLS();
  }
  let parsedData = JSON.parse(data);
  parsedData[itemName][subItemName] = value;
  localStorage.setItem(bookingKey, JSON.stringify(parsedData));
}

export function setItem(itemName, value) {
  var data = localStorage.getItem(bookingKey);
  if (data == null) {
    configureLS();
  }
  let parsedData = JSON.parse(data);
  parsedData[itemName] = value;

  localStorage.setItem(bookingKey, JSON.stringify(parsedData));
}

export function getAllBookingData() {
  let data = localStorage.getItem(bookingKey);
  if (data == null) {
    configureLS();
  }
  var parsedData = JSON.parse(data);
  return parsedData;
}

export function getRoomData() {
  let data = localStorage.getItem(bookingKey);
  if (data == null) {
    configureLS();
  }
  var parsedData = JSON.parse(data);
  return parsedData.room;
}

export function getTimeData() {
  let data = localStorage.getItem(bookingKey);
  if (data == null) {
    configureLS();
  }
  var parsedData = JSON.parse(data);
  return parsedData.time;
}
export function getPaymentData() {
  let data = localStorage.getItem(bookingKey);
  if (data == null) {
    configureLS();
  }
  var parsedData = JSON.parse(data);
  return parsedData.payment;
}
