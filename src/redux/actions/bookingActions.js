import * as actionTypes from "./actionTypes";
import {
  configureLS,
  getAllBookingData,
  setSubItem
} from "../../services/localStorageService";

export function getBookingSuccess(booking) {
  return { type: actionTypes.GET_BOOKING, payload: booking };
}

export function setBookingSuccess(booking) {
  return { type: actionTypes.ADD_BOOKING_STEP_DATA, payload: booking };
}

export function getBooking() {
  configureLS();
  return function(dispatch) {
    let getBookingData = getAllBookingData();
    return dispatch(getBookingSuccess(getBookingData));
  };
}

export function setBooking(name, subName, value) {
  configureLS();
  return function(dispatch) {
    setSubItem(name, subName, value);
    dispatch(setBookingSuccess(value));
  };
}
