import * as actionTypes from "../actions/actionTypes";
import initialStates from "./initialStates";

export default function bookingReducer(state = initialStates.booking, action) {
  switch (action.type) {
    case actionTypes.GET_BOOKING:
      return action.payload;
    default:
      return state;
  }
}
