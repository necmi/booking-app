import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import alertify from "alertifyjs";
import Steps from "../common/Steps";
import Spacer from "../common/Spacer";
import { Row, Col, Form, Input } from "reactstrap";
import { Link } from "react-router-dom";
import { FaChevronLeft } from "react-icons/fa";
import Cards from "react-credit-cards";
import CardInput from "../toolbox/CardInput";
import CvcInput from "../toolbox/CvcInput";
import ValidDateInput from "../toolbox/ValidDateInput";
import * as bookingActions from "../../redux/actions/bookingActions";
import * as validations from "../../services/bookingValidationService";
import { stepValidation } from "../../services/stepValidationService";

class PaymentComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cvc: "",
      expiry: "",
      focus: "",
      name: "",
      number: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    let validation = stepValidation("room");
    if (validation !== null && validation.length > 0) {
      this.props.history.push("/oda");
    }
  }

  componentDidMount() {
    this.props.actions.getBooking();
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.props.actions.setBooking("payment", name, value);
    this.props.actions.getBooking();
  }

  handleSubmit(event) {
    event.preventDefault();
    let validationResult = validations.paymentValidation(
      this.props.booking.payment.cvc,
      this.props.booking.payment.expiry,
      this.props.booking.payment.name,
      this.props.booking.payment.number
    );

    if (!validationResult.status) {
      validationResult.errors.map(error => {
        return alertify.error(error.text);
      });
      return;
    } else {
      let allValidation = validations.checkAllValidation();
      if (allValidation != null && allValidation.length > 0) {
        allValidation.map(error => {
          return alertify.error(error.text);
        });
      } else {
        console.log(this.props.booking);
        alertify.success("Ödeme işlemi Başarılı");
      }
    }
  }

  handleInputFocus = e => {
    this.setState({ focus: e.target.name });
  };

  render() {
    return (
      <div>
        <Steps location={this.props.location} />
        <Spacer size={25} />
        <Row>
          <Col>
            <Cards
              cvc={
                this.props.booking.payment !== undefined &&
                this.props.booking.payment.cvc !== undefined
                  ? this.props.booking.payment.cvc
                  : ""
              }
              expiry={
                this.props.booking.payment !== undefined &&
                this.props.booking.payment.expiry !== undefined
                  ? this.props.booking.payment.expiry
                  : ""
              }
              name={
                this.props.booking.payment !== undefined &&
                this.props.booking.payment.name !== undefined
                  ? this.props.booking.payment.name
                  : ""
              }
              number={
                this.props.booking.payment !== undefined &&
                this.props.booking.payment.number !== undefined
                  ? this.props.booking.payment.number
                  : ""
              }
              placeholders={{ name: "Ad Soyad" }}
              focused={this.state.focus}
            />
            <Spacer size={20} />
          </Col>
        </Row>

        <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col>
              <Input
                type="text"
                placeholder="Kart Üzerindeki İsim"
                value={
                  this.props.booking.payment !== undefined &&
                  this.props.booking.payment.name !== undefined
                    ? this.props.booking.payment.name
                    : ""
                }
                onChange={this.handleChange}
                onFocus={this.handleInputFocus}
                name="name"
              />
              <Spacer />
            </Col>
          </Row>
          <Row>
            <Col>
              <CardInput
                type="tel"
                placeholder="Kart Numarası"
                onChange={this.handleChange}
                onFocus={this.handleInputFocus}
                value={
                  this.props.booking.payment !== undefined &&
                  this.props.booking.payment.number !== undefined
                    ? this.props.booking.payment.number
                    : ""
                }
                name="number"
              />
            </Col>
            <Col>
              <ValidDateInput
                type="tel"
                placeholder="Son Kul. Tarihi"
                onChange={this.handleChange}
                onFocus={this.handleInputFocus}
                value={
                  this.props.booking.payment !== undefined &&
                  this.props.booking.payment.expiry !== undefined
                    ? this.props.booking.payment.expiry
                    : ""
                }
                name="expiry"
              />
              <Spacer />
            </Col>
          </Row>
          <Row>
            <Col>
              <CvcInput
                type="tel"
                placeholder="CVC"
                onChange={this.handleChange}
                onFocus={this.handleInputFocus}
                value={
                  this.props.booking.payment !== undefined &&
                  this.props.booking.payment.cvc !== undefined
                    ? this.props.booking.payment.cvc
                    : ""
                }
                name="cvc"
              />
            </Col>
          </Row>
          <Spacer size={25} />
          <Row>
            <Col>
              <Link block="true" className="btn btn-primary" to="/oda">
                <FaChevronLeft /> Geri
              </Link>
            </Col>
            <Col>
              <button className="btn btn-success payment-button">Ödeme</button>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    booking: state.bookingReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      getBooking: bindActionCreators(bookingActions.getBooking, dispatch),
      setBooking: bindActionCreators(bookingActions.setBooking, dispatch)
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentComp);
