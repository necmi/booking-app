import React, { Component } from "react";
import Spacer from "../common/Spacer";
import { Route, Switch, Redirect } from "react-router-dom";
import DateComp from "../date/DateComp";
import RoomComp from "../room/RoomComp";
import PaymentComp from "../payment/PaymentComp";
import NotFound from "../common/NotFound";
import { Container, Navbar, NavbarBrand } from "reactstrap";
import "./App.css";

export default class App extends Component {
  render() {
    return (
      <div>
        <Navbar color="dark" dark expand="md">
          <Container>
            <NavbarBrand>Booking App</NavbarBrand>
          </Container>
        </Navbar>
        <Spacer />
        <Container>
          <Switch>
            <Redirect exact from="/" to="tarih" />
            <Route exact path="/tarih" component={DateComp} />
            <Route exact path="/oda" component={RoomComp} />
            <Route exact path="/odeme" component={PaymentComp} />
            <Route component={NotFound} />
          </Switch>
        </Container>
      </div>
    );
  }
}
