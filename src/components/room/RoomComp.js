import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import alertify from "alertifyjs";
import Steps from "../common/Steps";
import { Form, FormGroup, Label, Row, Col } from "reactstrap";
import Spacer from "../common/Spacer";
import { Link } from "react-router-dom";
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";
import * as bookingActions from "../../redux/actions/bookingActions";
import * as validations from "../../services/bookingValidationService";
import { stepValidation } from "../../services/stepValidationService";
import CustomRadioInput from "../toolbox/CustomRadioInput";
class RoomComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      room: "",
      landscape: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    let validation = stepValidation("date");
    if (validation !== null && validation.length > 0) {
      this.props.history.push("/tarih");
    }
  }
  componentDidMount() {
    this.props.actions.getBooking();
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.props.actions.setBooking("room", name, value);
    this.props.actions.getBooking();
  }

  handleSubmit(event) {
    event.preventDefault();
    let validationResult = validations.roomValidation(
      this.props.booking.room.room,
      this.props.booking.room.landscape
    );

    if (!validationResult.status) {
      validationResult.errors.map(error => {
        return alertify.error(error.text);
      });
      return;
    } else {
      this.props.history.push("/odeme");
    }
  }

  render() {
    return (
      <div>
        <Steps location={this.props.location} />
        <Spacer size={25} />
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Row>
              <Col md="6">
                <Label>
                  Check-in Tarihi :{" "}
                  {this.props.booking.date !== undefined
                    ? this.props.booking.date.checkindate
                    : ""}
                </Label>
              </Col>
              <Col md="6">
                <Label>
                  Check-out Tarihi :{" "}
                  {this.props.booking.date !== undefined
                    ? this.props.booking.date.checkoutdate
                    : ""}
                </Label>
              </Col>
            </Row>
          </FormGroup>
          <FormGroup>
            <Row>
              <Col md="3">
                <Label>Oda Tipi:</Label>
              </Col>
              <Col md="3">
                <CustomRadioInput
                  id="room-standart"
                  name="room"
                  value="Standart"
                  checked={
                    this.props.booking.room !== undefined &&
                    this.props.booking.room.room === "Standart"
                  }
                  onChange={this.handleChange}
                  className="checkbox-input"
                  label="Standart"
                />
              </Col>
              <Col md="3">
                <CustomRadioInput
                  id="room-deluxe"
                  label="Deluxe"
                  name="room"
                  value="Deluxe"
                  checked={
                    this.props.booking.room !== undefined &&
                    this.props.booking.room.room === "Deluxe"
                  }
                  onChange={this.handleChange}
                  className="checkbox-input"
                />
              </Col>
              <Col md="3">
                <CustomRadioInput
                  id="room-suit"
                  label="Suit"
                  name="room"
                  value="Suit"
                  checked={
                    this.props.booking.room !== undefined &&
                    this.props.booking.room.room === "Suit"
                  }
                  onChange={this.handleChange}
                  className="checkbox-input"
                />
              </Col>
            </Row>
          </FormGroup>
          <Spacer size={16} />
          <FormGroup>
            <Row>
              <Col md="3">
                <Label>Manzara :</Label>
              </Col>
              <Col md="3">
                <Label className="custom-radio" check>
                  <CustomRadioInput
                    id="landscape-sea"
                    label="Deniz"
                    type="radio"
                    name="landscape"
                    value="Deniz"
                    checked={
                      this.props.booking.room !== undefined &&
                      this.props.booking.room.landscape === "Deniz"
                    }
                    onChange={this.handleChange}
                    className="checkbox-input"
                  />
                </Label>
              </Col>
              <Col md="3">
                <Label className="custom-radio" check>
                  <CustomRadioInput
                    id="landscape-kara"
                    label="Kara"
                    name="landscape"
                    value="Kara"
                    checked={
                      this.props.booking.room !== undefined &&
                      this.props.booking.room.landscape === "Kara"
                    }
                    onChange={this.handleChange}
                    className="checkbox-input"
                  />
                </Label>
              </Col>
              <Col></Col>
            </Row>
          </FormGroup>

          <Spacer size={20} />
          <FormGroup>
            <Row>
              <Col>
                <Link block="true" className="btn btn-primary" to="/tarih">
                  <FaChevronLeft /> Geri
                </Link>
              </Col>
              <Col>
                <button className="btn btn-primary next-button">
                  İleri <FaChevronRight />
                </button>
              </Col>
            </Row>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    booking: state.bookingReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      getBooking: bindActionCreators(bookingActions.getBooking, dispatch),
      setBooking: bindActionCreators(bookingActions.setBooking, dispatch)
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomComp);
