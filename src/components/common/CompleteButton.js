import React, { Component } from "react";
import { FaCheck } from "react-icons/fa";

const buttonStyle = {
  float: "right",
  marginTop: 0,
  backgroundColor: "transparent",
  border: "none",
  fontSize: 16,
  color: "#28a745",
  outline: "none"
};

export default class CloseButton extends Component {
  render() {
    return (
      <button onClick={this.props.onClick} style={buttonStyle}>
        <FaCheck />
      </button>
    );
  }
}
