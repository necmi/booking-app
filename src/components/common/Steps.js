import React, { Component } from "react";
import { ListGroup, ListGroupItem } from "reactstrap";
export default class Steps extends Component {
  render() {
    return (
      <ListGroup className="step-list" horizontal>
        <ListGroupItem active={this.props.location.pathname === "/tarih"}>
          Tarih
        </ListGroupItem>
        <ListGroupItem active={this.props.location.pathname === "/oda"}>
          Oda
        </ListGroupItem>
        <ListGroupItem active={this.props.location.pathname === "/odeme"}>
          Odeme
        </ListGroupItem>
      </ListGroup>
    );
  }
}
