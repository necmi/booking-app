import React, { Component } from "react";
import { FaTrashAlt } from "react-icons/fa";

const buttonStyle = {
  float: "right",
  marginTop: 0,
  backgroundColor: "transparent",
  border: "none",
  fontSize: 16,
  color: "#dc3545",
  outline: "none"
};

export default class DeleteButton extends Component {
  render() {
    return (
      <button onClick={this.props.onClick} style={buttonStyle}>
        <FaTrashAlt />
      </button>
    );
  }
}
