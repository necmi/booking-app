import React, { Component } from "react";

export default class Spacer extends Component {
  render() {
    return (
      <div
        style={{
          marginTop: this.props.size || 10,
          marginBottom: this.props.size || 10
        }}
      ></div>
    );
  }
}
