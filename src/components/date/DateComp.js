import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import alertify from "alertifyjs";
import Steps from "../common/Steps";
import { Form, FormGroup, Label, Input, Row, Col } from "reactstrap";
import Spacer from "../common/Spacer";
import { FaChevronRight } from "react-icons/fa";
import * as bookingActions from "../../redux/actions/bookingActions";
import * as validations from "../../services/bookingValidationService";

class DateComp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checkindate: "",
      checkoutdate: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.actions.getBooking();
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.props.actions.setBooking("date", name, value);
    this.props.actions.getBooking();
  }

  handleSubmit(event) {
    window.s = event;
    event.preventDefault();
    let validationResult = validations.dateValidation(
      this.props.booking.date.checkindate,
      this.props.booking.date.checkoutdate
    );
    if (!validationResult.status) {
      validationResult.errors.map(error => {
        return alertify.error(error.text);
      });
      return;
    } else {
      this.props.history.push("/oda");
    }
  }

  render() {
    return (
      <div>
        <Steps location={this.props.location} />
        <Spacer size={25} />
        <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col md="4">
              <Label className="custom-label" for="checkindate">
                Check-in Tarihi:
              </Label>
            </Col>
            <Col>
              <FormGroup>
                <Input
                  type="date"
                  name="checkindate"
                  id="checkindate"
                  value={
                    this.props.booking.date !== undefined &&
                    this.props.booking.date.checkindate !== undefined
                      ? this.props.booking.date.checkindate
                      : ""
                  }
                  placeholder="Check-in Tarihi"
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="4">
              <Label className="custom-label" for="checkoutdate">
                Check-out Tarihi:
              </Label>
            </Col>
            <Col>
              <FormGroup>
                <Input
                  type="date"
                  name="checkoutdate"
                  id="checkoutdate"
                  value={
                    this.props.booking.date !== undefined &&
                    this.props.booking.date.checkoutdate !== undefined
                      ? this.props.booking.date.checkoutdate
                      : ""
                  }
                  placeholder="Check-out Tarihi"
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Row>
              <Col>
                <button className="btn btn-primary next-button">
                  İleri <FaChevronRight />
                </button>
              </Col>
            </Row>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    booking: state.bookingReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      getBooking: bindActionCreators(bookingActions.getBooking, dispatch),
      setBooking: bindActionCreators(bookingActions.setBooking, dispatch)
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DateComp);
