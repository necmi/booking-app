import React from "react";
import InputMask from "react-input-mask";
import { Input } from "reactstrap";

export default function CvvInput(props) {
  return (
    <InputMask mask="99/99" {...props}>
      {inputProps => <Input {...inputProps} />}
    </InputMask>
  );
}
