import React from "react";
import InputMask from "react-input-mask";
import { Input } from "reactstrap";

export default function CardInput(props) {
  return (
    <InputMask mask="9999 9999 9999 9999" {...props}>
      {inputProps => <Input {...inputProps} />}
    </InputMask>
  );
}
