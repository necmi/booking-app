import React from "react";

const CustomRadioInput = props => {
  return (
    <div className="custom-radio-button">
      <input {...props} type="radio" />
      <label htmlFor={props.id}>{props.label}</label>
    </div>
  );
};

export default CustomRadioInput;
