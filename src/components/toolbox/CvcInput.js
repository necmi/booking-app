import React from "react";
import InputMask from "react-input-mask";
import { Input } from "reactstrap";

export default function CvcInput(props) {
  return (
    <InputMask mask="999" {...props}>
      {inputProps => <Input {...inputProps} />}
    </InputMask>
  );
}
