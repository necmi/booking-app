import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./index.css";
import App from "./components/root/App";
import * as serviceWorker from "./serviceWorker";
import conigureStore from "./redux/reducers/configureStore";
import { BrowserRouter } from "react-router-dom";
import alertify from "alertifyjs";

import "bootstrap/dist/css/bootstrap.min.css";
import "react-credit-cards/es/styles-compiled.css";
import "alertifyjs/build/css/alertify.min.css";
import "alertifyjs/build/css/themes/bootstrap.min.css";

alertify.set("notifier", "position", "top-right");
const store = conigureStore();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
